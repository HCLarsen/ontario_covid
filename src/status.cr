require "json"

require "./record"

module OntarioCovid
  class Status
    getter records = {} of Time => OntarioCovid::Record

    struct Response
      include JSON::Serializable

      getter result : Result
    end

    struct Result
      include JSON::Serializable

      getter records : Array(Record)
    end

    def initialize(records)
      records_array = [] of OntarioCovid::Record

      records.each_cons_pair do |record1, record2|
        record1.cases = record1.total_cases - record2.total_cases
        if record1.deaths == 0
          record1.deaths = record1.total_deaths - record2.total_deaths
        end
        record1.resolved = record1.total_resolved - record2.total_resolved
        record1.positivity_delta = record1.percent_positive - record2.percent_positive
        records_array << record1
      end
      records_array << records.last

      records_array = records_array.map_with_index do |e, i|
        if i < records_array.size - 1
          e.cases_delta = e.cases - records_array[i+1].cases
          e.deaths_delta = e.deaths - records_array[i+1].deaths
        end
        e
      end

      cases = records_array.map { |record| record.cases }
      records_array.each_with_index do |record, index|
        record.seven_day_average = (cases[index,7].sum / 7).round.to_i
        @records[record.date] = record
      end
    end

    def self.from_json(json : String)
      response = Response.from_json(json)
      records = response.result.records

      self.new(records)
    end

    def latest : OntarioCovid::Record
      @records.first_value
    end

    def latest_date : Time
      @records.first_key
    end

    def seven_day_averages : Array(Int32)
      @records.values[0..-7].map { |record| record.seven_day_average }
    end
  end
end
