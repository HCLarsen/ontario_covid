require "http"

require "./status"

# TODO: Write documentation for `OntarioCovid`
module OntarioCovid
  VERSION = "0.4.4"

  def self.get_recent_status(limit = 8) : OntarioCovid::Status
    url = "https://data.ontario.ca/api/3/action/datastore_search?limit=#{limit}&sort=_id%20desc&resource_id=ed270bb8-340b-41f9-a7c6-e8ef587e6d11"
    data = HTTP::Client.get(url)

    if data.status_code == 200
      OntarioCovid::Status.from_json(data.body)
    else
      raise "Server Error #{data.status_code}"
    end
  end

  def self.get_status_for(date : Time) : OntarioCovid::Status
    offset = (self.last_thursday - date).days + 1

    url = "https://data.ontario.ca/api/3/action/datastore_search?sort=_id+desc&offset=#{offset}&limit=8&resource_id=ed270bb8-340b-41f9-a7c6-e8ef587e6d11"
    data = HTTP::Client.get(url)

    if data.status_code == 200
      OntarioCovid::Status.from_json(data.body)
    else
      raise "Server Error #{data.status_code}"
    end
  end

  private def self.last_thursday : Time
    day_of_week = Time.local.day_of_week
    diff = day_of_week.value - Time::DayOfWeek::Thursday.value

    if diff < 0
      diff += 7
    end

    Time.local.shift(days: diff * -1)
  end
end
