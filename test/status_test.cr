require "minitest/autorun"

require "/../src/status.cr"

class StatusTest < Minitest::Test
  def test_parses_json
    json = File.read("test/files/response.json")
    status = OntarioCovid::Status.from_json(json)

    assert_equal Time.local(2021, 4, 10), status.latest_date
    assert_equal [3371, 2826], status.seven_day_averages

    latest = status.latest
    assert_equal 382152, latest.total_cases
    assert_equal 3813, latest.cases
    assert_equal 19, latest.deaths
    assert_equal 3371, latest.seven_day_average

    assert_equal -414, latest.cases_delta
    assert_equal 1, latest.deaths_delta
    assert_equal "0.2", latest.positivity_delta.to_s[0..2]
  end

  def test_parses_new_json
    json = File.read("test/files/response22.json")
    status = OntarioCovid::Status.from_json(json)

    assert_equal Time.local(2022, 2, 22), status.latest_date
    assert_equal [2003, 2048, 2109, 2152, 2252, 2333, 2458, 2548, 2619, 2666, 2754, 2792, 2954, 3083, 3189, 2890], status.seven_day_averages
    assert_equal 16, status.seven_day_averages.size

    latest = status.latest
    assert_equal 1090101, latest.total_cases
    assert_equal 1282, latest.cases
    assert_equal 9, latest.deaths
    assert_equal 2003, latest.seven_day_average
    assert_equal 6.9, latest.percent_positive

    fourth = status.records.values[3]
    assert_equal 14, fourth.deaths
  end
end
