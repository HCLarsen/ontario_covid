require "minitest/autorun"

require "timecop"
require "webmock"

require "/../src/ontario_covid.cr"

class OntarioCovidTest < Minitest::Test
  def test_gets_data
    WebMock.wrap do
      WebMock.stub(:get, "https://data.ontario.ca/api/3/action/datastore_search?limit=8&sort=_id%20desc&resource_id=ed270bb8-340b-41f9-a7c6-e8ef587e6d11").to_return(status: 200, body: File.read("test/files/response.json"))

      status = OntarioCovid.get_recent_status

      assert_equal Time.local(2021, 4, 10), status.latest_date

      latest = status.latest
      assert_equal 382152, latest.total_cases
      assert_equal 3813, latest.cases
      assert_equal 19, latest.deaths
    end
  end

  def test_gets_larger_data
    WebMock.wrap do
      WebMock.stub(:get, "https://data.ontario.ca/api/3/action/datastore_search?limit=22&sort=_id%20desc&resource_id=ed270bb8-340b-41f9-a7c6-e8ef587e6d11").to_return(status: 200, body: File.read("test/files/response22.json"))

      status = OntarioCovid.get_recent_status(22)
      assert_equal 1282, status.latest.cases
    end
  end

  def test_handles_error
    WebMock.wrap do
      WebMock.stub(:get, "https://data.ontario.ca/api/3/action/datastore_search?limit=8&sort=_id%20desc&resource_id=ed270bb8-340b-41f9-a7c6-e8ef587e6d11").to_return(status: 502, body: File.read("test/files/502error.html"))

      error = assert_raises do
        status = OntarioCovid.get_recent_status
      end

      assert_equal "Server Error 502", error.message
    end
  end

  def test_gets_older_data
    WebMock.wrap do
      Timecop.travel(Time.local(2022, 8, 22, 0, 0, 0)) do
        WebMock.stub(:get, "https://data.ontario.ca/api/3/action/datastore_search?sort=_id+desc&offset=230&limit=8&resource_id=ed270bb8-340b-41f9-a7c6-e8ef587e6d11").to_return(status: 200, body: File.read("test/files/endof2021.json"))

        WebMock.stub(:get, "https://data.ontario.ca/api/3/action/datastore_search?sort=_id+desc&offset=595&limit=8&resource_id=ed270bb8-340b-41f9-a7c6-e8ef587e6d11").to_return(status: 200, body: File.read("test/files/endof2020.json"))

        status = OntarioCovid.get_status_for(Time.local(2021, 12, 31))

        assert_equal Time.local(2021, 12, 31), status.latest_date
        latest = status.latest
        assert_equal 756361, latest.total_cases

        status = OntarioCovid.get_status_for(Time.local(2020, 12, 31))
        assert_equal Time.local(2020, 12, 31), status.latest_date
        latest = status.latest
        assert_equal 182159, latest.total_cases
      end
    end
  end
end
