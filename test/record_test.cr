require "minitest/autorun"

require "/../src/record.cr"

class RecordTest < Minitest::Test
  def test_parses_json
    json = File.read("test/files/record.json")
    record = OntarioCovid::Record.from_json(json)

    assert_equal 430, record.id
    assert_equal 6.5, record.percent_positive
    assert_equal 6.5, record.percent_positive
    assert_equal 382152, record.total_cases
    assert_equal 343622, record.total_resolved
    assert_equal 7531, record.total_deaths
  end

  def test_parses_with_null_deaths
    json = File.read("test/files/new_record.json")
    record = OntarioCovid::Record.from_json(json)

    assert_equal 12256, record.total_deaths
    assert_equal 8, record.deaths
    refute record.hospitalized
  end
end
